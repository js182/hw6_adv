const ipBtn = document.getElementById('find-ip-btn');
const addressInfoDiv = document.getElementById('address-info');

ipBtn.addEventListener('click', async () => {
  try {
    const {ip} = await fetch('https://api.ipify.org/?format=json').then(res => res.json());
    const {
      continent,
      country,
      regionName,
      city,
      region
    } = await fetch(`http://ip-api.com/json/${ip}`).then(res => res.json());

    const addressInfo = `
      <p>Continent: ${continent}</p> 
      <p>Country: ${country}</p>
      <p>Region: ${regionName}</p>
      <p>City: ${city}</p>
      <p>Region: ${region}</p>
    `;
    addressInfoDiv.innerHTML = addressInfo;
  } catch (err) {
    addressInfoDiv.innerHTML = '<p>Unable to find address information</p>';
  }
});
